import React, { Component } from 'react';
import { Dimensions } from 'react-native';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createDrawerNavigator } from 'react-navigation-drawer';
import { createStackNavigator } from 'react-navigation-stack';

import HomeScreen from '../screens/Home';
import SignIn from '../screens/SignIn';
import SignUp from '../screens/SignUp';
import MyAccount from '../screens/MyAccount';
import PublicationDetail from '../screens/PublicationDetail';

const WIDTH = Dimensions.get('window').width;

const DrawerConfig = {
  drawerWidth: WIDTH * 0.83,
  drawerBackgroundColor: '#3B5998',
  contentOptions: {
    activeTintColor: '#fff',
    labelStyle: {
      color: 'white',
    },
  },
};

class Hidden extends Component {
  render() {
    return null;
  }
}
const AppDrawer = createDrawerNavigator(
  {
    SignIn: {
      screen: SignIn,
      navigationOptions: {
        drawerLabel: <Hidden />,
      }
    },
    SignUp: {
      screen: SignUp,
      navigationOptions: {
        drawerLabel: <Hidden />,
      }
    },
    Home: {
      screen: HomeScreen,
      navigationOptions: {
        title: 'Inicio'
      }
    },
    MyAccount: {
      screen: MyAccount,
    },
    PublicationDetail: {
      screen: PublicationDetail,
      navigationOptions: {
        drawerLabel: <Hidden />
      }
    },
  },
  DrawerConfig,
);

export default createAppContainer(AppDrawer);
