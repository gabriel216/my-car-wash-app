import React, {Component} from 'react';
import AppButton from '../components/appButton';
import {
  TouchableHighlight,
  View,
  Text,
  ScrollView,
  StyleSheet,
  Button,
  Image,
  Modal,
} from 'react-native';
import {Icon} from 'native-base';
import DatePicker from 'react-native-datepicker';
import MainImage from '../../assets/images/carWash.jpg';
import Vendor from '../../assets/images/vendor.gif';
import HeaderMenu from '../components/MenuButton';

class PublicationDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {date: new Date()};
  }
  state = {
    modalVisible: false,
  };
  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }

  render() {
    console.log(this.props);
    const {navigation} = this.props;
    return (
      <ScrollView>
        <HeaderMenu navigation={navigation} />
        <View style={styles.containerImage}>
          <Image
            source={MainImage}
            style={styles.image}
            onPress={() => navigation.navigate('Home')}
          />
        </View>
        <View style={styles.containerInfo}>
          <Text>Servicio de Autolavado Maipú</Text>
          <Text>
            Con sus formatos autoservicio y automático, Service Car APP es la
            cadena de lavado de automóviles más grande de Chile. Prestamos
            servicios de lavado en más de 135 estaciones de Arica a Coyhaique y
            contamos con más de 150 equipos de última generación. Nuestras
            máquinas logran excelentes resultados y niveles de limpieza con los
            diferentes programas acordes a tus necesidades.
          </Text>
          <AppButton
            bgColor="#18B748"
            title="Reservar servicio"
            action={() => this.setModalVisible(true)}
          />
          <Modal
            animationType="slide"
            transparent={false}
            visible={this.state.modalVisible}
            onRequestClose={() => {
              Alert.alert('Modal has been closed.');
            }}>
				<View style={styles.mainContainerModal}>
					<View style={styles.containerModal}>
					<TouchableHighlight
						onPress={() => {
						this.setModalVisible(!this.state.modalVisible);
						}}
						style={styles.iconContent}>
						<Text>
						<Icon name="close" size={24} color="black" />
						</Text>
					</TouchableHighlight>
					<Text style={styles.titleModal}>Reservar fecha y hora del servicio</Text>
					<View>
						<DatePicker
						style={{width: 200}}
						date={this.state.date}
						mode="date"
						placeholder="select date"
						format="YYYY-MM-DD"
						minDate={new Date()}
						confirmBtnText="Confirm"
						cancelBtnText="Cancel"
						customStyles={{
							dateIcon: {
							position: 'absolute',
							left: 15,
							top: 4,
							marginLeft: 30,
							},
							dateInput: {
							marginLeft: 40,
							},
						}}
						onDateChange={date => {
							this.setState({date: date});
						}}
						/>
					</View>
					<View style={styles.vendorContainer}>
						<Image source={Vendor} style={styles.vendor}/>
					</View>
					<AppButton
						bgColor="#18B748"
						title="Guardar fecha"
						action={() => this.setModalVisible(false)}
					/>
					</View>
				</View>
          </Modal>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
	mainContainerModal: {
		backgroundColor: '#CBCBCB',
		height:800,
	},
  containerModal: {
    alignSelf: 'stretch',
    textAlign: 'center',
    padding: 20,
	margin: 20,
  },
  iconContent: {
    marginLeft: 'auto',
  },
  containerImage: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  titleModal: {
	  fontSize: 25,
	  padding: 20,
  },
  containerInfo: {
    textAlign: 'center',
    paddingLeft: 30,
    paddingRight: 30,
    paddingTop: 10,
  },
  image: {
    width: 250,
    height: 250,
    resizeMode: 'contain',
  },
  title: {
    fontSize: 15,
    width: 200,
    height: 160,
    padding: 20,
  },
  description: {
    fontSize: 13,
    width: 200,
    height: 160,
    padding: 20,
  },
  vendorContainer: {
    alignSelf: 'stretch',
	  paddingRight: 40,
  },
  vendor: {
	paddingTop:20,
	width: 300,
	height: 300,
  }
});

export default PublicationDetail;
