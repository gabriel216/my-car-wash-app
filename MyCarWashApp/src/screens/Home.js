import React, { Component } from 'react';
import { View, Text, StyleSheet, Button } from 'react-native';
import Publication from './Publication';
import HeaderMenu from '../components/MenuButton';

class Home extends Component {
    constructor (props) {
        super(props);
    }
    render() {
        console.log(this.props)
        const { navigation } = this.props
        return( 
            <View style={styles.container}>
                <HeaderMenu navigation={navigation}/>                
                <Publication navigation={navigation}/>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    }
})

export default Home;