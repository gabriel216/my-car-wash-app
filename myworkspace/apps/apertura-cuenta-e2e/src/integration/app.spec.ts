import { getGreeting } from '../support/app.po';

describe('apertura-cuenta', () => {
  beforeEach(() => cy.visit('/'));

  it('should display welcome message', () => {
    getGreeting().contains('Welcome to apertura-cuenta!');
  });
});
