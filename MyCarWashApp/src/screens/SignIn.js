import React, {Component} from 'react';
import {View, Text, StyleSheet, TextInput} from 'react-native';
import AppButton from '../components/appButton';
import HeaderMenu from '../components/MenuButton';
import {Icon} from 'native-base';
import {Input} from 'react-native-elements';
class SignIn extends Component {
  render() {
    const {navigation} = this.props;
    return (
      <View style={styles.containerInfo}>
        <Text style={styles.title}>Service Car App</Text>
        <View style={styles.containerInfo}>
          <Text style={styles.text}>Correo electrónico</Text>
          <TextInput
            placeholder="Correo"
            style={styles.input}
            leftIcon={
              <Icon
                name="mail"
                size={24}
                color="black"
                style={{paddingRight: 7}}
              />
            }
          />
          <Text style={styles.text}>Contraseña</Text>
          <TextInput
            placeholder="Contraseña"
            type="Password"
            style={styles.input}
            leftIcon={
              <Icon
                name="mail"
                size={24}
                color="black"
                style={{paddingRight: 7}}
              />
            }
          />
          <AppButton
            bgColor="#0BBEDE"
            title="Iniciar sesión"
            action={() => navigation.navigate('Home')}
          />
          <AppButton
            bgColor="#18B748"
            title="Registrarse"
            action={() => navigation.navigate('SignUp')}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#3B5998',
  },
  containerInfo: {
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 50,
    alignSelf: 'stretch',
    textAlign: 'center',
    backgroundColor: '#3B5998',
    height: 1000,
  },
  title: {
    fontSize: 30,
    textAlign: 'center',
    color: '#fff',
  },
  text: {
    color: '#fff',
    paddingTop: 20,
  },
  input: {
    height: 40,
    fontSize: 13,
    padding: 4,
    color: '#fff',
    borderColor: '#0BBEDE',
    borderBottomWidth: 1,
  },
});

export default SignIn;
