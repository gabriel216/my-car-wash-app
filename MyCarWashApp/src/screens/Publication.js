import React, {Component} from 'react';
import {
  TouchableHighlight,
  View,
  Text,
  StyleSheet,
  Button,
  Image,
} from 'react-native';
import Profile from '../../assets/images/carWash.jpg';

class Publication extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    console.log(this.props)
    const { navigation } = this.props;
    return (
      <TouchableHighlight
        style={styles.container}
        onPress={() => navigation.navigate('PublicationDetail', {otherRoute:'Home'})}>
        <View style={styles.containerInfo}>
          <Image source={Profile} style={styles.image} />
          <Text style={styles.title}>Servicio de Autolavado Maipú {'\n'}</Text>
          {/* <Text styles={styles.description}>
            Servicio a domicilio, con personal profesional y confiable
          </Text> */}
        </View>
      </TouchableHighlight>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignSelf: 'stretch',
    marginTop: 10,
    marginLeft: 30,
    marginRight: 30,
    height: 150,
  },
  containerInfo: {
    flexDirection: 'row',
    paddingTop: 10,
  },
  image: {
    marginTop: 0,
    width: 120,
    height: 120,
    resizeMode: 'contain',
  },
  title: {
    fontSize: 15,
    width: 180,
    height: 160,
    padding: 20,
  },
  description: {
    fontSize: 13,
    width: 200,
    height: 160,
    padding: 20,
  },
});

export default Publication;
