import React, { Component } from 'react';
import { Button } from 'react-native-elements'
import { Dimensions } from 'react-native';

export default class AppButton extends Component {
    render() {
        const {action, title, bgColor} = this.props;
        return (
            <Button
                onPress={action}
                buttonStyle={{
                    backgroundColor: bgColor,
                    height: 45,
                    borderColor: 'transparent',
                    borderWidth: 0,
                    borderRadius: 5,
                    marginBottom: 5,
                    marginTop: 40,
                    alignItems: "center",
                    justifyContent: "center",
                }}
                title={title}
                text={title}
            >
            </Button>
        )
    }
}
